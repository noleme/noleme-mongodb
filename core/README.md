# Noleme MongoDB

_Last updated for v0.2.1_

This library is meant as a collection of utility classes for facilitating the instantiation and basic configuration of a MongoDB client.

This code was originally part of the noleme-database repository, which is now deprecated.

_Note: This library is considered as "in beta" and as such significant API changes may occur without prior warning._

## I. Installation

Add the following in your `pom.xml`:

```xml
<dependency>
    <groupId>com.noleme</groupId>
    <artifactId>noleme-mongodb</artifactId>
    <version>0.2.1</version>
</dependency>
```

## II. Notes on Structure and Design

_TODO_

## III. Usage

_TODO_

## IV. Dev Installation

### A. Pre-requisites

This project will require you to have the following:

* Git (versioning)
* Maven (dependency resolving, publishing and packaging) 

### B. Setup

_TODO_
