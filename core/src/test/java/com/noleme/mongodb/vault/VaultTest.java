package com.noleme.mongodb.vault;

import com.noleme.mongodb.configuration.MongoDBConfiguration;
import com.noleme.vault.Vault;
import com.noleme.vault.exception.VaultException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Field;
import java.util.Map;

import static com.noleme.mongodb.configuration.MongoDBConfiguration.*;

/**
 * @author Pierre LECERF (pierre@noleme.com)
 * Created on 25/05/2021
 */
public class VaultTest
{
    @BeforeEach
    void setup()
    {
        clearEnv();
    }

    @Test
    void buildConf_adjuster() throws VaultException
    {
        Vault vault = Vault.with("vault/noleme-mongodb/client-config.yml", variables -> variables
            .set("mongodb.connect_timeout", 123)
            .set("mongodb.connection_max_idle", 234)
            .set("mongodb.connection_max_life", 345)
            .set("mongodb.connections_per_host", 456)
            .set("mongodb.socket_timeout", 567)
            .set("mongodb.socket_keep_alive", false)
            .set("mongodb.ssl_enable", true)
            .set("mongodb.ssl_verify_host", true)
        );

        MongoDBConfiguration config = vault.instance(MongoDBConfiguration.class, "mongodb.configuration");

        Assertions.assertEquals("localhost", config.getString(HOST));
        Assertions.assertEquals(27017, config.getInteger(PORT));
        Assertions.assertEquals("database", config.getString(DATABASE));
        Assertions.assertFalse(config.has(USER));
        Assertions.assertFalse(config.has(PASSWORD));
        Assertions.assertEquals(123, config.getInteger(CONNECT_TIMEOUT));
        Assertions.assertEquals(234, config.getInteger(CONNECTION_MAX_IDLE));
        Assertions.assertEquals(345, config.getInteger(CONNECTION_MAX_LIFE));
        Assertions.assertEquals(456, config.getInteger(CONNECTIONS_PER_HOST));
        Assertions.assertEquals(567, config.getInteger(SOCKET_TIMEOUT));
        Assertions.assertFalse(config.getBoolean(SOCKET_KEEP_ALIVE));
        Assertions.assertTrue(config.getBoolean(SSL_ENABLE));
        Assertions.assertTrue(config.getBoolean(SSL_VERIFY_HOST));
    }

    @Test
    void buildConf_env() throws VaultException
    {
        setEnv("MONGODB_HOST", "0.0.0.0");
        setEnv("MONGODB_PORT", "27020");
        setEnv("MONGODB_DATABASE", "my_database");
        setEnv("MONGODB_CONNECT_TIMEOUT", "123");
        setEnv("MONGODB_SSL_ENABLE", "true");

        Vault vault = Vault.with("vault/noleme-mongodb/client-config.yml");

        MongoDBConfiguration config = vault.instance(MongoDBConfiguration.class, "mongodb.configuration");

        Assertions.assertEquals("0.0.0.0", config.getString(HOST));
        Assertions.assertEquals(27020, config.getInteger(PORT));
        Assertions.assertEquals("my_database", config.getString(DATABASE));
        Assertions.assertFalse(config.has(USER));
        Assertions.assertFalse(config.has(PASSWORD));
        Assertions.assertEquals(123, config.getInteger(CONNECT_TIMEOUT));
        Assertions.assertTrue(config.getBoolean(SSL_ENABLE));
    }

    /**
     * Nasty hack for altering the env variable snapshot ; does not impact the actual env environment, only its in-memory representation.
     * This produces a warning for illegal reflective access, should not be an issue in this context.
     *
     * @param name
     * @param value
     */
    @SuppressWarnings("unchecked")
    public static void setEnv(String name, String value)
    {
        try {
            Map<String, String> env = System.getenv();
            Field field = env.getClass().getDeclaredField("m");
            field.setAccessible(true);
            ((Map<String, String>) field.get(env)).put(name, value);
        }
        catch (IllegalAccessException | NoSuchFieldException e) {
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    /**
     * Nasty hack for altering the env variable snapshot ; does not impact the actual env environment, only its in-memory representation.
     * This produces a warning for illegal reflective access, should not be an issue in this context.
     */
    @SuppressWarnings("unchecked")
    public static void clearEnv()
    {
        try {
            Map<String, String> env = System.getenv();
            Field field = env.getClass().getDeclaredField("m");
            field.setAccessible(true);
            ((Map<String, String>) field.get(env)).clear();
        }
        catch (IllegalAccessException | NoSuchFieldException e) {
            throw new RuntimeException(e.getMessage(), e);
        }
    }
}
