package com.noleme.mongodb.configuration;

import com.mongodb.MongoClientOptions;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static com.noleme.mongodb.configuration.MongoDBConfiguration.*;

/**
 * @author Pierre LECERF (pierre@noleme.com)
 * Created on 30/05/2021
 */
public class MongoDBConfigurationTest
{
    @Test
    void builderTest()
    {
        MongoDBConfiguration config = new MongoDBConfiguration()
            .set(CONNECT_TIMEOUT, 123)
            .set(CONNECTION_MAX_IDLE, 234)
            .set(CONNECTION_MAX_LIFE, 345)
            .set(CONNECTIONS_PER_HOST, 456)
            .set(SOCKET_TIMEOUT, 567)
            .set(SOCKET_KEEP_ALIVE, false)
            .set(SSL_ENABLE, true)
            .set(SSL_VERIFY_HOST, true)
        ;

        MongoClientOptions options = MongoClientOptionsBuilder.build(config);

        Assertions.assertEquals(123, options.getConnectTimeout());
        Assertions.assertEquals(234, options.getMaxConnectionIdleTime());
        Assertions.assertEquals(345, options.getMaxConnectionLifeTime());
        Assertions.assertEquals(456, options.getConnectionsPerHost());
        Assertions.assertEquals(567, options.getSocketTimeout());
        Assertions.assertFalse(options.isSocketKeepAlive());
        Assertions.assertTrue(options.isSslEnabled());
        Assertions.assertFalse(options.isSslInvalidHostNameAllowed());
    }
}
