package com.noleme.mongodb.configuration;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Map;
import java.util.Properties;

/**
 * @author Pierre Lecerf (pierre@noleme.com)
 * Created on 22/05/15.
 */
public class MongoDBConfigurationLoader
{
    /**
     *
     * @param path
     * @return
     * @throws MongoDBConfigurationLoaderException
     */
    public static MongoDBConfiguration load(String path) throws MongoDBConfigurationLoaderException
    {
        return load(path, new MongoDBConfiguration());
    }

    /**
     *
     * @param map
     * @return
     * @throws MongoDBConfigurationLoaderException
     */
    public static MongoDBConfiguration load(Map<String, Object> map) throws MongoDBConfigurationLoaderException
    {
        Properties props = new Properties();
        map.forEach((key, value) -> {
            if (value != null)
                props.setProperty(key, (String) value);
        });
        return ConfigurationLoader.load(props, new MongoDBConfiguration());
    }

    /**
     *
     * @param path
     * @return
     * @throws MongoDBConfigurationLoaderException
     */
    public static MongoDBConfiguration load(String path, MongoDBConfiguration conf) throws MongoDBConfigurationLoaderException
    {
        InputStream in = null;
        try {
            in = new FileInputStream(path);
            Properties props = new Properties();
            props.load(in);

            return ConfigurationLoader.load(props, conf);
        }
        catch (FileNotFoundException e) {
            throw new MongoDBConfigurationLoaderException("No file could be found for path "+path+".", e);
        }
        catch (IOException e) {
            throw new MongoDBConfigurationLoaderException("An error occurred while attempting to parse file in path "+path+".", e);
        }
        finally {
            if (in != null)
            {
                try {
                    in.close();
                }
                catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
