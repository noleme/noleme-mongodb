package com.noleme.mongodb.configuration;

/**
 * @author Pierre Lecerf (pierre@noleme.com)
 * Created on 22/05/15.
 */
public class MongoDBConfigurationLoaderException extends Exception
{
    public MongoDBConfigurationLoaderException(String message) { super(message); }
    public MongoDBConfigurationLoaderException(String message, Throwable e) { super(message, e); }
}