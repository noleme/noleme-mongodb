package com.noleme.mongodb.configuration;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * @author Pierre Lecerf (pierre@noleme.com)
 * Created on 27/01/2018
 */
public abstract class Configuration <T extends Configuration<T>>
{
    protected Map<String, Object> properties = new HashMap<>();

    /**
     *
     */
    public Configuration()
    {

    }

    /**
     *
     * @param property
     * @param value
     */
    public Configuration(String property, Object value)
    {
        this.properties.put(property, value);
    }

    @SuppressWarnings("unchecked")
    public T set(String property, Object value)
    {
        this.properties.put(property, value);
        return (T)this;
    }

    @SuppressWarnings("unchecked")
    public T unset(String property)
    {
        this.properties.remove(property);
        return (T)this;
    }

    public boolean has(String property)
    {
        return this.properties.containsKey(property);
    }

    public Object get(String property)
    {
        return this.properties.get(property);
    }

    public String getString(String property)
    {
        return (String)this.properties.get(property);
    }

    public Integer getInteger(String property)
    {
        return (Integer)this.properties.get(property);
    }

    public Double getDouble(String property)
    {
        return (Double)this.properties.get(property);
    }

    public Boolean getBoolean(String property)
    {
        return (Boolean)this.properties.get(property);
    }

    /**
     *
     * @return
     */
    public Set<Map.Entry<String, Object>> entries()
    {
        return this.properties.entrySet();
    }
}
