package com.noleme.mongodb.configuration;

import com.mongodb.MongoClientOptions;

/**
 * @author Pierre Lecerf (pierre@noleme.com)
 * Created on 22/05/15.
 */
public class MongoClientOptionsBuilder
{
    public static MongoClientOptions build(MongoDBConfiguration conf)
    {
        MongoClientOptions.Builder b = MongoClientOptions.builder();

        if (conf.has(MongoDBConfiguration.CONNECT_TIMEOUT))
            b.connectTimeout((int)conf.get(MongoDBConfiguration.CONNECT_TIMEOUT));
        if (conf.has(MongoDBConfiguration.CONNECTION_MAX_IDLE))
            b.maxConnectionIdleTime((int)conf.get(MongoDBConfiguration.CONNECTION_MAX_IDLE));
        if (conf.has(MongoDBConfiguration.CONNECTION_MAX_LIFE))
            b.maxConnectionLifeTime((int)conf.get(MongoDBConfiguration.CONNECTION_MAX_LIFE));
        if (conf.has(MongoDBConfiguration.CONNECTIONS_PER_HOST))
            b.connectionsPerHost((int)conf.get(MongoDBConfiguration.CONNECTIONS_PER_HOST));
        if (conf.has(MongoDBConfiguration.SOCKET_TIMEOUT))
            b.socketTimeout((int)conf.get(MongoDBConfiguration.SOCKET_TIMEOUT));
        if (conf.has(MongoDBConfiguration.SOCKET_KEEP_ALIVE))
            b.socketKeepAlive((boolean)conf.get(MongoDBConfiguration.SOCKET_KEEP_ALIVE));
        if (conf.has(MongoDBConfiguration.SSL_ENABLE))
            b.sslEnabled((boolean)conf.get(MongoDBConfiguration.SSL_ENABLE));
        if (conf.has(MongoDBConfiguration.SSL_VERIFY_HOST))
            b.sslInvalidHostNameAllowed(!(boolean)conf.get(MongoDBConfiguration.SSL_VERIFY_HOST));

        return b.build();
    }
}
