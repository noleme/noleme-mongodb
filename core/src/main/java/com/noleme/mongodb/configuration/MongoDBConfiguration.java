package com.noleme.mongodb.configuration;

/**
 * @author Pierre Lecerf (pierre@noleme.com)
 * Created on 22/05/15.
 */
public class MongoDBConfiguration extends Configuration<MongoDBConfiguration>
{
    public static final String HOST = "host";
    public static final String PORT = "port";
    public static final String DATABASE = "database";
    public static final String USER = "user";
    public static final String PASSWORD = "password";
    public static final String CONNECT_TIMEOUT = "connect_timeout";
    public static final String CONNECTION_MAX_IDLE = "connection_max_idle";
    public static final String CONNECTION_MAX_LIFE = "connection_max_life";
    public static final String CONNECTIONS_PER_HOST = "connections_per_host";
    public static final String SOCKET_TIMEOUT = "socket_timeout";
    public static final String SOCKET_KEEP_ALIVE = "socket_keep_alive";
    public static final String SSL_ENABLE = "ssl_enable";
    public static final String SSL_VERIFY_HOST = "ssl_verify_host";

    /**
     *
     */
    public MongoDBConfiguration()
    {
        super();
    }

    /**
     *
     * @param path
     * @throws MongoDBConfigurationLoaderException
     */
    public MongoDBConfiguration(String path) throws MongoDBConfigurationLoaderException
    {
        this();
        MongoDBConfigurationLoader.load(path, this);
    }
}
