package com.noleme.mongodb;

/**
 * @author Pierre Lecerf (pierre@noleme.com)
 * Created on 19/09/2014.
 */
public class MongoDBClientException extends Exception
{
    public MongoDBClientException(String message) { super(message); }
    public MongoDBClientException(String message, Throwable e) { super(message, e); }
}
