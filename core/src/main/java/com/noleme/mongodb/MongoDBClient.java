package com.noleme.mongodb;

import com.mongodb.*;
import com.mongodb.client.MongoDatabase;
import com.noleme.mongodb.configuration.MongoClientOptionsBuilder;
import com.noleme.mongodb.configuration.MongoDBConfiguration;
import com.noleme.mongodb.configuration.MongoDBConfigurationLoader;
import com.noleme.mongodb.configuration.MongoDBConfigurationLoaderException;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Pierre Lecerf (pierre@noleme.com)
 * Created on 19/09/2014.
 */
public class MongoDBClient implements AutoCloseable
{
    protected MongoClient client;
    protected DB database;
    protected MongoDatabase mongoDatabase;

    /**
     *
     * @param database
     * @throws MongoDBClientException
     */
    @Deprecated
    public MongoDBClient(String database) throws MongoDBClientException
    {
        this.setup(new MongoDBConfiguration()
            .set(MongoDBConfiguration.HOST, "localhost")
            .set(MongoDBConfiguration.PORT, 27017)
            .set(MongoDBConfiguration.DATABASE, database)
        );
    }

    /**
     *
     * @param database
     * @param host
     * @param port
     * @throws MongoDBClientException
     */
    @Deprecated
    public MongoDBClient(String database, String host, Integer port) throws MongoDBClientException
    {
        this.setup(new MongoDBConfiguration()
            .set(MongoDBConfiguration.HOST, host)
            .set(MongoDBConfiguration.PORT, port)
            .set(MongoDBConfiguration.DATABASE, database)
        );
    }

    /**
     *
     * @param database
     * @param path
     * @throws MongoDBClientException
     */
    @Deprecated
    public MongoDBClient(String database, String path) throws MongoDBClientException
    {
        try {
            this.setup(
                MongoDBConfigurationLoader.load(path)
                .set(MongoDBConfiguration.DATABASE, database)
            );
        }
        catch (MongoDBConfigurationLoaderException e) {
            throw new MongoDBClientException("An error occurred while attempting to load the configuration from path "+path+".", e);
        }
    }

    /**
     *
     * @param conf
     * @throws MongoDBClientException
     */
    public MongoDBClient(MongoDBConfiguration conf) throws MongoDBClientException
    {
        this.setup(conf);
    }

    /**
     *
     * @param conf
     * @throws MongoDBClientException
     */
    protected void setup(MongoDBConfiguration conf) throws MongoDBClientException
    {
        Logger mongoLogger = Logger.getLogger("org.mongodb.driver");
        mongoLogger.setLevel(Level.SEVERE);

        if (!conf.has(MongoDBConfiguration.HOST))
            throw new MongoDBClientException("No \""+MongoDBConfiguration.HOST+"\" property could be found in the provided configuration.");
        if (!conf.has(MongoDBConfiguration.PORT))
            throw new MongoDBClientException("No \""+MongoDBConfiguration.PORT+"\" property could be found in the provided configuration.");
        if (!conf.has(MongoDBConfiguration.DATABASE))
            throw new MongoDBClientException("No \""+MongoDBConfiguration.DATABASE+"\" property could be found in the provided configuration.");

        String host = conf.getString(MongoDBConfiguration.HOST);
        int port = conf.getInteger(MongoDBConfiguration.PORT);
        String database = conf.getString(MongoDBConfiguration.DATABASE);
        String user = conf.has(MongoDBConfiguration.USER) ? conf.getString(MongoDBConfiguration.USER) : null;
        String password = conf.has(MongoDBConfiguration.PASSWORD) ? conf.getString(MongoDBConfiguration.PASSWORD) : null;

        MongoClientOptions opt = MongoClientOptionsBuilder.build(conf);

        try {
            ServerAddress addr = new ServerAddress(host, port);
            if (user != null && password != null)
            {
                MongoCredential credential = MongoCredential.createCredential(user, database, password.toCharArray());
                this.client = new MongoClient(addr, credential, opt);
            }
            else
                this.client = new MongoClient(addr, opt);

            this.database = this.client.getDB(database);
            this.mongoDatabase = this.client.getDatabase(database);

            this.database.getCollection(database).findOne();
        }
        catch (MongoTimeoutException e) {
            this.close();
            throw new MongoDBClientException("The host "+host+":"+port+" could not be reached. (MongoTimeoutException)", e);
        }
    }

    /**
     * Conditionally closes the MongoClient connexion. If the client is not instantiated, nothing is performed.
     */
    public void close()
    {
        if (this.client != null)
        {
            this.client.close();
            this.database = null;
            this.mongoDatabase = null;
        }
    }

    /**
     *
     * @return
     */
    public DB db()
    {
        return this.database;
    }

    /**
     * New mongo driver API database object (3.x)
     * @return
     */
    public MongoDatabase mongoDb()
    {
        return this.mongoDatabase;
    }

}
