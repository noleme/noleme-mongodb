package com.noleme.mongodb.vault;

import com.noleme.mongodb.MongoDBClient;
import com.noleme.vault.Vault;
import com.noleme.vault.exception.VaultException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 * @author Pierre LECERF (pierre@noleme.com)
 * Created on 25/05/2021
 */
public class VaultTest
{
    @Test
    void buildConf_withoutOverrides() throws VaultException
    {
        Vault vault = Vault.with("vault/noleme-mongodb-test/client.yml");

        MongoDBClient client = vault.instance(MongoDBClient.class, "mongodb.client");

        Assertions.assertEquals("database", client.db().getName());
    }

    @Test
    void buildConf_scopedImports() throws VaultException
    {
        Vault vault = Vault.with("vault/noleme-mongodb-test/scoped_import.yml");

        MongoDBClient clientA = vault.instance(MongoDBClient.class, "mongodb.client.a");
        MongoDBClient clientB = vault.instance(MongoDBClient.class, "mongodb.client.b");

        Assertions.assertEquals("database_a", clientA.db().getName());
        Assertions.assertEquals("database_b", clientB.db().getName());
    }
}
