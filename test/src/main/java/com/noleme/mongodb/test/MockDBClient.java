package com.noleme.mongodb.test;

import com.mongodb.MongoClient;
import com.mongodb.ServerAddress;
import com.noleme.mongodb.MongoDBClient;
import com.noleme.mongodb.MongoDBClientException;
import com.noleme.mongodb.configuration.MongoDBConfiguration;
import de.bwaldvogel.mongo.MongoServer;
import de.bwaldvogel.mongo.backend.memory.MemoryBackend;

/**
 * @author Pierre LECERF (pierre@noleme.com)
 * Created on 17/01/2020
 */
public class MockDBClient extends MongoDBClient
{
    /**
     *
     * @param configuration
     * @throws MongoDBClientException
     */
    public MockDBClient(MongoDBConfiguration configuration) throws MongoDBClientException
    {
        super(configuration);
    }

    @Override
    protected void setup(MongoDBConfiguration conf) throws MongoDBClientException
    {
        if (!conf.has(MongoDBConfiguration.DATABASE))
            throw new MongoDBClientException("No \"" + MongoDBConfiguration.DATABASE + "\" property could be found in the provided configuration.");

        String database = conf.getString(MongoDBConfiguration.DATABASE);
        MongoServer server = new MongoServer(new MemoryBackend());

        this.client = new MongoClient(new ServerAddress(server.bind()));

        this.database = client.getDB(database);
        this.mongoDatabase = client.getDatabase(database);
    }

    @Override
    public void close()
    {
        this.database = null;
    }
}
