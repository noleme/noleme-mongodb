# Noleme MongoDB Test

_Last updated for v0.1_

This library is meant as a collection of utility classes for facilitating the testing of noleme-mongodb backed components.

_Note: This library is considered as "in beta" and as such significant API changes may occur without prior warning._

## I. Installation

Add the following in your `pom.xml`:

```xml
<dependency>
    <groupId>com.noleme</groupId>
    <artifactId>noleme-mongodb-test</artifactId>
    <version>0.2</version>
    <scope>test</scope>
</dependency>
```

## II. Notes on Structure and Design

_TODO_

## III. Usage

_TODO_

## IV. Dev Installation

### A. Pre-requisites

This project will require you to have the following:

* Git (versioning)
* Maven (dependency resolving, publishing and packaging) 

### B. Setup

_TODO_
